# Job Offers

We are hiring! 🎉

Due to the success of multiple of our projects, we are frequently looking for new profile to join us. Please consult our current offers into the [job offers](#available-positions) section.

## About us

Gracia Media Web is a «Startup Laboratory» designing, building, launching and maintaining websites and mobile applications over the years.

Our team is composed of creative and motivated persons with different backgrounds, experiences and origin. We are multicultural, multilingual, open-minded and proud of it.

Our offices are located into the sunny Barcelona where our teams are working on-site on different floors of the same building. We all share the same rooftop terrace where you can have a drink and meet your co-workers.

## Available positions

All the following positions are currently available:

- [Fullstack Developer (Symfony)](offers/fullstack-developer-symfony.md)
- [Backend Developer (Symfony)](offers/backend-developer-symfony.md)
- [Frontend Developer](offers/frontend-developer.md)
- [AWS DevOps Engineer](offers/aws-devops-engineer.md)

## Why you should join us?

- 💰 Competitive salary
- 📅 30 days of paid holidays
- 🕙 Flexible working time
- 🏠 Home office possibilities (hybrid remote work)
- 📚 Learning programs and career evolution opportunities
- 💬 Language classes in Spanish, English and French
- 🍎 Free organic fruits, drinks and snacks deliveries
