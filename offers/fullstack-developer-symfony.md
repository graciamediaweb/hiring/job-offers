# Fullstack Developer (Symfony)

We are looking for a Symfony Fullstack Developer to join our Barcelona team.

## The job position

You will be in charge of the development, the testing and the maintaining of applications and micro-applications based on Symfony (PHP).

### The mission

- Development of new applications and micro-applications
- Improvement and maintaining of existing projects
- Implementation of external APIs and payment solutions
- Unit testing
- Pair coding with other members of your team
- Participating to learning program and sharing knowledge

## The candidate

The candidate must have a significant experience as a Fullstack Developer, a good ability to work independently but also as a part of a team.

### Skills requirements

- More than 3 years of experience as Fullstack Developer
- A working experience with Symfony
- Language required: PHP, Javascript, Twig, Html, Css
- Agile methodology, Git, Gitflow, CI/CD,…
- Must speak and write in English

### Bonus skills

- An experience in Payment Solution implementation
- Knowledge into AWS services and solutions
- Additional languages are welcome
- Speak Spanish and/or French
