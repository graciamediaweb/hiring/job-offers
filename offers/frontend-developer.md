# Frontend Developer

We are looking for a Frontend Developer to join our Barcelona team.

## The job position

You will be in charge of the development, the testing and the maintaining of applications.

### The mission

- Development of front-end applications (web and mobile)
- Improvement and maintaining of existing projects
- Pair coding with other members of your team
- Participating to learning program and sharing knowledge

## The candidate

### Skills requirements

- More than 2 years of experience as Frontend Developer
- A serious knowledge in HTML, SASS, JavaScript, Bootstrap,…
- Problem-solving skills and open to feedback
- Agile methodology, Git, Gitflow, CI/CD,…
- Must speak and write in English

### Bonus skills

- An experience in mobile application development
- Speak Spanish and/or French
