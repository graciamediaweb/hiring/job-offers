# QA Analyst / Tester

We are looking for a QA Analyst to join our Barcelona team.

## The job position

You will be responsible for testing our applications, identifying problems, and suggesting improvements.

### The mission

- Design and perform tests and define corrective actions with the development team
- Work closely with the scrum team and be in charge of validate the delivery of each sprints
- Manage and update the defect backlog

## The candidate

The candidate must have a significant experience as a Quality Assurance Analyst.

### Skills requirements

- More than 2 years of experience in a testing environment
- Knowledge of Agile software development lifecycle
- Problem solver with great communication skills
- Must speak and write in English

### Bonus skills

- Knowledge in test automation (Selenium, Appium, Cypress)
- Development language: Javascript, Python
- Speak Spanish and/or French
