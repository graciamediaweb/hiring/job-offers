# AWS DevOps Engineer

We are looking for a AWS DevOps Engineer to join our Barcelona team.

## The job position

You will be in charge of the deployment and the maintaining of a cloud based infrastructure on AWS.

### The mission

- Deploy new cloud based infrastructures on AWS
- Maintaining our existing infrastructure and planify their migrations
- Development of cloud native micro-services (Serverless, AWS Lambda)
- Automatisation and improvement of our Gitlab CI/CD pipelines

## The candidate

The candidate must have a significant experience as a DevOps, a good ability to work independently but also as a part of a team.

### Skills requirements

- More than 2 years of experience in cloud based infrastructure deployment
- Knowledges into the following AWS products & services: EC2, EBS, RDS, Dynamo DB, Lambda, S3, EFS,…
- Development of Serverless micro-applications
- Agile methodology, Git, Gitflow, CI/CD,…
- Must speak and write in English

### Bonus skills

- Test-driven development (TDD)
- Speak Spanish and/or French
