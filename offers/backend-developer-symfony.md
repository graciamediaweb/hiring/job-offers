# Backend Developer (Symfony)

We are looking for a Symfony Backend Developer to join our Barcelona team.

## The job position

You will be in charge of the development, the testing and the maintaining of applications and micro-applications based on PHP (Symfony).

### The mission

- Development of new applications and micro-applications
- Improvement and maintaining of existing projects
- Implementation of external APIs and payment solutions
- Unit testing implementation and documentation redaction
- Pair coding with other members of your team
- Participating to learning program and sharing knowledge

## The candidate

The candidate must have an experience as a Backend Developer with the latest Symfony versions. He should have a good ability to work independently but also as a part of a team.

### Skills requirements

- More than 3 years of experience as Backend Developer with Symfony (latest versions)
- Agile methodology, Git, Gitflow, CI/CD,…
- Must speak and write in English

### Bonus skills

- An experience in Payment Solution implementation
- Knowledge into AWS services and solutions
- Additional coding languages are welcome
- Test-driven development (TDD)
- Speak Spanish and/or French
